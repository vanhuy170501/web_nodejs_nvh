const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const app = express();
const session = require('express-session');
const methodOverride = require('method-override');
const exphbs  = require('express-handlebars');
const port = 9000;
const db = require("./config/db");

app.use(session({
    secret: 'abcxyz123',
    resave: false,
    saveUninitialized: true
}));

mongoose.set('useCreateIndex', true);

app.use(express.json());

app.use(methodOverride('_method'));

app.use(express.urlencoded({ extended: true }));
db.connect();

const route = require("./routes");

app.use(express.static(path.join(__dirname,"public")))

app.engine('hbs', exphbs({
    extname: ".hbs",
    helpers: {
        add1: (a,b) => a + b,
    }
}));
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, "resource","views"))

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
});

route(app);