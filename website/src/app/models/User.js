const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("express-session");

const User = new Schema({
    fullname: String,
    email: String,
    password: String,
    password_confirmation: String
}, {
    timestamps: true
});

module.exports = mongoose.model("User", User);