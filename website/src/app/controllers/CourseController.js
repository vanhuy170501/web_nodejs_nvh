const Course = require("../models/Course")
const {mongooseToObject} = require("../../util/mongoose")
class CourseController {

    show(req, res, next) {
        Course.findOne({slug: req.params.slug})
            .then(course => {
                res.render("courses/show", {course: mongooseToObject(course)});
            })
            .catch(next)
    }

    create(req, res) {
        res.render("courses/create")
    }

    store(req, res) {
        const FormData = req.body;
        FormData.image = `https://img.youtube.com/vi/${FormData.videoid}/sddefault.jpg`;
        const course = new Course(FormData);
        course.save()
        .then(() => res.redirect("/"))
        .catch(error =>{

        });
    }
    edit(req, res, next) {
        Course.findById(req.params.id)
            .then(course => res.render("courses/edit", {
                course: mongooseToObject(course)
            }))
            .catch(next);
    }
    update(req, res, next){
        Course.updateOne({ _id: req.params.id }, req.body)
            .then(() => res.redirect('/me/stored/courses'))
            .catch(next);
    }
    delete(req, res, next){
        Course.deleteOne({ _id: req.params.id })
            .then(() => res.redirect('back'))
            .catch(next);
    }
}
module.exports = new CourseController;