const Course = require("../models/Course");
const User = require("../models/User");
const {multipleToObject} = require("../../util/mongoose");
const jwt = require("jsonwebtoken");
const JSAlert = require("js-alert");
class SiteController {
    home(req, res, next){
        var token = req.session.token;
        var name = req.session.fullname;
        console.log(token)
        Course.find({})
            .then(courses => {
                res.render("home", {
                    token,
                    name,
                    courses: multipleToObject(courses)
                });
            })
            .catch(next)

    }
    search(req, res){
        res.render("search");
    }
    register(req, res){
        res.render("register");
    }
    getData(req, res){
        var fullname = req.body.fullname;
        var email = req.body.email;
        var password = req.body.password;
        var password_confirmation = req.body.password_confirmation;

        User.findOne({
            email: email
        })
            .then(data => {
                if(data){
                    res.redirect("register")
                }
                else {
                    return User.create({
                        fullname: fullname,
                        email: email,
                        password: password,
                        password_confirmation: password_confirmation
                    })
                        .then((data) => res.redirect("/login"))
                }
            })
            .catch(err => {
                res.status(404);
            })

    }
    login(req, res){
        var token = req.session.token;
        console.log(token);
        res.render("login", {
            token
        });
    }
    checkData(req, res){
        var email = req.body.email;
        var password = req.body.password;
        User.findOne({
            email: email,
            password: password
        })
            .then(data => {
                  if(data){
                    var token = jwt.sign({
                        _id: data._id
                    }, "secret password");
                    var name = data.fullname;
                    req.session.fullname = name;
                    req.session.token = token;
                    res.redirect("/")
                }
                else {
                    res.redirect("/login");
                }
            })
    }
    logout(req, res){
        req.session.destroy(function(err){
            if(err){
                console.log(err);
                Response.send("Đăng xuất thất bại");
            }
            else
            {
                res.redirect("/login");
            }
        });
    }
}

module.exports = new SiteController;