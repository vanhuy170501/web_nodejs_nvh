const Course = require("../models/Course")
const {multipleToObject} = require("../../util/mongoose")
class MeController {
    storedCourses(req, res, next){
        Course.find({})
            .then(course => res.render("me/stored-courses", {
                course: multipleToObject(course)
            }))
            .catch(next)
    }
}

module.exports = new MeController;