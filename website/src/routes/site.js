const express = require("express");
const router = express.Router();

const siteController = require("../app/controllers/SiteController");

router.get("/search", siteController.search);

router.get("/register", siteController.register);

router.post("/getData", siteController.getData);

router.get("/login", siteController.login);

router.post("/checkData", siteController.checkData);

router.get("/logout", siteController.logout);

router.get("/", siteController.home);

module.exports = router;